# Simple Java Client #

A simple Java text-based stream client encapsulated in a Swing graphical user interface. Useful for troubleshooting server protocols.

### SimpleConsole ###

The UI element of this, the actual console, is reusable with its own interface.

![Window.png](https://bitbucket.org/repo/5L76ag/images/2843641775-Window.png)