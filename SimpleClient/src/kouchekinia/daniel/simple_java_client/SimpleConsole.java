package kouchekinia.daniel.simple_java_client;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SimpleConsole extends JFrame{

	private static final long serialVersionUID = 1L;
	private JTextArea textBox;
	
	public SimpleConsole(String title, SimpleConsoleInterface consoleInterface) {
		super(title);
		this.setSize(500, 700);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		//Scroll pane with text area
		textBox = new JTextArea();
		textBox.setEditable(false);
		textBox.setLineWrap(true);
		
		panel.add(new JScrollPane(textBox), BorderLayout.CENTER);
		
		//Text Input
		JTextField textInput = new JTextField();
		textInput.addKeyListener(new KeyListener(){

			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == 10){
					consoleInterface.stringSubmitted(textInput.getText());
					textInput.setText("");
				}				
			}

		});
		panel.add(textInput, BorderLayout.SOUTH);
		
		this.add(panel);
		this.setVisible(true);
		textInput.requestFocus();
	}
	
	public void println(String string){
		textBox.setText(textBox.getText() + string + "\n");
	}
	
	public void lineBreak(){
		this.println("");
	}
}
