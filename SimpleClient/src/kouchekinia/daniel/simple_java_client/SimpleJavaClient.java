package kouchekinia.daniel.simple_java_client;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JOptionPane;

public class SimpleJavaClient implements SimpleConsoleInterface{
	
	private SimpleConsole console;
	private String lastSpoke = null;
	
	private String hostname;
	private int port;
	
	private PrintWriter out;
	private BufferedReader in;
	private Socket socket = null;
	
	public static void main(String[] args) {
		new SimpleJavaClient();
	}
	
	public SimpleJavaClient() {
		String hostnameAndPort = JOptionPane.showInputDialog(null, "Example: 'localhost:3000'", "Enter Server Information", JOptionPane.QUESTION_MESSAGE);
		String[] hostnameAndPortArray = hostnameAndPort.split(":");
		
		try{
			hostname = hostnameAndPortArray[0];
			port = Integer.parseInt(hostnameAndPortArray[1]);
		}catch(ArrayIndexOutOfBoundsException e){
			JOptionPane.showMessageDialog(null, "Server information incomplete.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		try {
			socket = new Socket(hostname, port);
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Connection Error: " + e.getMessage(), "Fatal Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		console = new SimpleConsole(hostnameAndPort, this);
		
		Thread incomingThread = new Thread(new Runnable(){

			@Override
			public void run() {
				while(true){
					try {
						serverPrintln(in.readLine());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		});
		incomingThread.start();
		
		console.addWindowListener(new WindowListener(){

			@Override
			public void windowOpened(WindowEvent e) {}

			@Override
			public void windowClosing(WindowEvent e) {
				closeConnection();
			}

			@Override
			public void windowClosed(WindowEvent e) {}

			@Override
			public void windowIconified(WindowEvent e) {}

			@Override
			public void windowDeiconified(WindowEvent e) {}

			@Override
			public void windowActivated(WindowEvent e) {}

			@Override
			public void windowDeactivated(WindowEvent e) {}
			
		});
	}

	@Override
	public void stringSubmitted(String string) {
		clientPrintln(string);
		out.println(string);
	}
	
	public void clientPrintln(String string){
		if(lastSpoke == "server"){
			lastSpoke = "client";
			console.lineBreak();
		}
		console.println("CLIENT: " + string);
		lastSpoke = "client";
	}
	
	public void serverPrintln(String string){
		if(lastSpoke == "client"){
			lastSpoke = "server";
			console.lineBreak();
		}
		
		console.println("SERVER: " + string);
		lastSpoke = "server";
	}
	
	public void closeConnection(){
		out.flush();
		out.close();
		try {
			in.close();
			socket.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
